#!/usr/bin/env bash

/usr/local/bin/docker-entrypoint.sh generate -i _build/$1/openapi.yml -g spring -c projects/$1/config.yml -o generated/$1